import './Css/InfoView.scss';
import React from 'react';

function InfoView(props) {
    const { isShow, onClickClose } = props;

    if (!isShow) return null;

    return (
        <div className="info-view modal is-active">
            <div className="modal-background" onClick={onClickClose}></div>
            <div className="info-view__content modal-content has-text-centered">
                <div className="columns is-mobile">
                    <div className="column">
                        <a href="https://www.cs.psu.ac.th/?fbclid=IwAR03S8QC0eizKjWSQhpaV95mWSMcMyrCOi7t2Z7QWo1u25_ysmnS7uS2myQ" target="_blank" rel="noopener noreferrer">COMSCI PSU</a>
                    </div>
                    <div className="column">
                        <a href="https://github.com/ExpDev07/coronavirus-tracker-api" target="_blank" rel="noopener noreferrer">Data API Github</a>
                    </div>
                    <div className="column">
                        <a href="http://covid19.rajavithi.go.th/th_index.php?fbclid=IwAR0LsQDSCFRCuJ1-Oq1XKIlj7E7ad2Q6UTa2RKZ3HVh8lW6BuGrbu7_7sUE" target="_blank" rel="noopener noreferrer">แบบประเมินความเสี่ยงของ สธ.</a>
                    </div>
                </div>
                <p className="is-size-7">&copy; 2020 COMSCI PSU</p>
            </div>
            <button className="modal-close is-large" onClick={onClickClose}></button>
        </div>
    );
}

export default InfoView;